= Pràctica Servidor DHCP

== Part 1

=== Apartat 1: Instal·lació del servidor DHCP a Farnsworth

==== Exercici 1: Captura del contingut del fitxer /etc/default/isc-dhcp-server

image::Imatges/Fitxer_isc_dhcp_server.png[]

==== Apart d'això, hem de modificar el fitxer principal de configuració del servidor DHCP amb les opcions que volem.

* Quin és el nom de l'arxiu de configuració del servei DHCP? Quan modifiquem l'arxiu, quina instrucció haurem d'executar per a què el servidor recarregui la nova configuració?

    El nom de l'arxiu de configuració és /etc/dhcp/dhcpd.conf i la instrucció per a recarregar la nova configuració és: sudo systemctl restart isc-dhcp.server.service

==== Exercici 2: Inserir una captura del registre del sistema mostrant els missatges que llença el servidor DHCP

image::Imatges/Registre_sistema_servidor_dhcp.png[]

* Ruta completa a l'arxiu de configuració

    /etc/dhcp/dhcpd.conf
    
* Instrucció utilitzada per reiniciar el servidor

    La instrucció per reiniciar és: sudo systemctl restart isc-dhcp-server.service
    
* Inseriu una captura del registre del sistema mostrant els missatges que llença el servidor DHCP (sudo cat /var/log/syslog | grep dhcpd
    
image::Imatges/Registre_syslog_dhcpd.png[]

* Explica el significat dels missatges de la captura anterior. Què estem veient?

    Estem veient com el DHCP intenta donar ip als PC connectats en la xarxa interna. A part també ens mostra que el servei està funcionant correctament.
    
=== Apartat 2: Servidor DHCP a la mateixa xarxa

==== Exercici 1: Configurar el servidor DHCP per tal que doni IP als ordinadors de la xarxa 172.30.1.0/24 (captura del arxiu /etc/dhcp/dhcpd.conf)

image::Imatges/dhcpd_conf.png[]

=== Apartat 3: Configuració de la màquina client PC1 (a la mateixa xarxa)

==== Exercici 1: Mostrar configuració xarxa de PC1

image::Imatges/netplan_PC1.png[]

image::Imatges/ip_a_PC1.png[]


* A Farnsworth seguiu els canvis al fitxer de registre del sistema amb tail -f /var/log/syslog

==== Exercici 2: Demanar nova ip a PC1 amb dhclient

image::Imatges/dhclient_PC1.png[]

==== Exercici 3: Captura les últimes línies del syslog de Farnsworth

image::Imatges/syslog_Farns.png[]

==== Exercici 4: Captura de pantalla amb la sortida del tcpdump.

* Observeu el tràfic generat a l'apartat anterior utilitzant el tcpdump a Farnsworth

image::Imatges/tcpdump_Farns.png[]

==== Exercici 5: Validar ip de PC1

image::Imatges/ip_a_PC1_2.png[]

==== Exercici 6: Validar porta d'enllaç

image::Imatges/ip_route_show.png[]

==== Exercici 7: Servidor DNS

image::Imatges/resolv_conf.png[]

==== Exercici 8: Captura de la instrucció

* Forceu al client la renovació d'adreça. Des del servidor, localitzeu l'arxiu on s'emmagatzema el registre de les cessions DHCP, i trobeu el fragment on s'ha registrat l'operació anterior. Ressalta la ip i l'hora de cessió i l'hora de caducitat de la cessió.

    L'arxiu on és registren les cessions DHCP està a /var/lib/dhcp/dhcp.leases

image::Imatges/dhcp_leases.png[]

==== Exercici 9: Captura MAC PC1

image::Imatges/mac_PC1.png[]

== Part 2

=== Apartat 1: Reserva d'adreces IP

==== Exercici 1: Captura configuració

* SERVIDOR_WEB: Té la MAC 00:0c:76:8b:c4:16. Volem que se li assigni la IP 172.30.1.5. 

* La màquina SERVIDOR_WEB ha de rebre el servidor DNS 8.8.8.8 i la porta d’enllaç 10.0.0.1. 

* PC1 ha de rebre la IP 172.30.1.8. 

image::Imatges/dhcpd_conf_2.png[]

==== Exercici 2: Verificació ip a PC1

image::Imatges/ip_a_PC1_3.png[]

=== Apartat 2: Grups d'adreces i clients registrats

==== Exercici 1: Captura modificacions arxiu configuració

* DHCP assignara:

    El rang 172.30.1.30 a 172.30.1.150 a les màquines registrades.
    El rang 172.30.1.151 a 172.30.1.254 a les màquines no registrades
    
* Per totes les màquines d'aquesta subxarxa, porta enllaç 172.30.1.1 i el DNS 172.30.1.1 i 172.30.1.2

image::Imatges/dhcpd_conf_3.png[]

==== Exercici 2: Captura ip renovada PC1

image::Imatges/ip_a_PC1_4.png[]

==== Exercici 3: Captura ip renovada PC2

image::Imatges/ip_a_PC2.png[]

== Part 3

=== Apartat 1: Configuració de PC1 i PC2 a una xarxa diferent


Volem ara traslladar els ordinadors PC1 i PC2 a la xarxa 172.30.2.0/24. Tindràs que configurar les seves interfícies internes a VirtualBox amb el nom intnet2

* Afegiu al servidor DHCP la configuració de la xarxa 172.30.2.0/24:

. EL servei DHCP repartirà adreces IP en el rang 172.30.2.100 – 172.30.2.254 (ambdues incloses).

. La porta d’enllaç serà 172.30.2.1.

* Esborra de la configuració qualsevol referència a la MAC de PC1 per evitar conflictes.

==== Exercici 1: Captura de la nova configuració

image::Imatges/dhcpd_conf_4_v2.png[]

==== Exercici 2: Comprovació de la IP de PC1 i PC2.

* IP PC1

image::Imatges/ip_a_PC1_5_v2.png[]

* IP PC2

image::Imatges/ip_a_PC2_2_v2.png[]

=== Apartat 7: Configuració d’un servidor secundari

Si la nostra xarxa utilitza DHCP per donar IP als clients, el servidor DHCP es
converteix en un punt de fallada única: si el servidor falla, tota la xarxa
deixa de funcionar.


Per aquest motiu és molt habitual crear no un, sinó dos servidors DHCP, de
manera que si un d’ells cau, l’altra pugui seguir concedint configuracions de
xarxa als clients.


El segon servidor s’anomena un servidor secundari i el mecanisme que permet
passar d’un a l’altre en cas de problemes s’anomena failover.


Anem a muntar un servidor secundari per a la nostra xarxa. El servidor
s’anomenarà Hermes i tindrà la IP 172.30.1.2 per la primera interfície interna i la 172.30.2.2 per la segona.


Per tal que els dos servidors es puguin comunicar correctament, és
necessari que tinguin el seu rellotge sincronitzat. Per aconseguir-ho,
instal·larem el paquet ntp, que proporciona un daemon que manté el
rellotge del servidor sincronitzat a través de servidor de temps d’Internet.

==== Exercici 3: Instal·lació del NTP a Farnsworth i a Hermes.

* Farnsworth

image::Imatges/ntp_install.png[]

* Hermes

image::Imatges/ntp_install_2.png[]

Crearem una configuració de failover anomenada dhcp-failover. Aquest nom
és arbitrari i s’utilitza per fer-ne referència si necessitem tenir més d’una
configuració de failover al mateix servidor.


Abans de crear la configuració, però, indicarem per quins clients la
utilitzarem. Per fer això només hem d’afegir la directiva
failover peer "dhcp-failover"; a tots els pools que tinguem definits.

==== Exercici 4: Demostració que el servidor DHCP s’està executant a Hermes.

image::Imatges/dhcpd_conf_Hermes.png[]

Ara, cal dir a cadascun dels dos servidors DHCP que tenen un company. Això ho
fem afegint una secció failover peer a la configuració.


Aquest és un exemple de configuració:

    failover peer "dhcp-failover" {
      primary;
      address 10.1.0.1;
      port 647;
      peer address 10.1.0.2;
      peer port 847;
      max-response-delay 60;
      max-unacked-updates 10;
      load balance max seconds 3;
      mclt 3600;
      split 128;
    }

Aquesta configuració inclou:

. Si el servidor DHCP és primari o secundari (primary, secondary).


. L’adreça IP del servidor i del seu company (address, peer address).


. Els ports que utilitzaran per comunicar-se entre ells (port, peer port).


. Temps que ha de passar per considerar que un servidor ha caigut i cal
intercanviar-los (max-response-delay, max-unacked-updated).


. Temps màxim que un dels servidors por estendre una concessió a un client
sense que ho sàpiga el seu company (mclt: maximum client lead time). Només
es posa al servidor primari.


. Divisió entre el servidor primari i secundari a efecte de balancejar la
càrrega, sobre 256 (split). 128 significa que es reparteixen la càrrega a
mitges. Número més grans signifiquen més càrrega pel primari, i més petits més
càrrega pel secundari. Només s’especifica al servidor primari.


. Temps màxim d’espera abans de resoldre una petició que, segons la
configuració de càrrega, era per l’altre servidor (load balance max seconds).

Adapta la configuració al nostre cas particular i afegeix-la a Farnsworth.

==== Exercici 5: Captura de la configuració de failover a Farnsworth.

image::Imatges/dhcpd_conf_5.png[]

Finalment, copiarem la mateixa configuració de _failover_ a _Hermes_, amb només dos petits canvis:

* Declararem _Hermes_ com a servidor secundari.

* No inclourem les opcions _mclt_ i _split_

==== Exercici 6: Captura de la configuració de failover a Hermes.

image::Imatges/dhcpd_conf_Hermes_2.png[]

Ja podem reiniciar els servidors DHCP!

==== Exercici 7: Nova configuració del DHCP relay.

* Farnsworth

image::Imatges/Farnsworth_status_dhcp_service.png[]

* Hermes

image::Imatges/Hermes_status_dhcp_service.png[]

Per comprovar que la configuració està funcionant podem apagar el servidor primari, utilitzar PC1 per demanar una IP (recorda canviar el nom de la interfície a VirtualBox), arrencar el servidor primari de nou, i veure com el _leasse_ de PC1 apareix a la llista de concessions de _Farnsworth_.

==== Exercici 8: Demostració que el failover funciona

* Màquina Farnsworth apagada

image::Imatges/Farnsworth_apagada.png[]

* PC1 demana IP

image::Imatges/PC1_dhclient.png[]

* PC1 rep IP de Hermes

image::Imatges/ip_a_PC1_6.png[]

* Llista concessions Farnsworth

image::Imatges/dhcp_leases_2.png[]

